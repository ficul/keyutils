#!/usr/bin/env python

import hashlib
import sys
import argparse
from mnemonic import Mnemonic

parser = argparse.ArgumentParser()
memo = Mnemonic("english")
phrase = ""

parser.add_argument("-v", dest="verbose", action="store_true", default=False)
parser.add_argument("-f", dest="format", action="store_true", default=False)

args = parser.parse_args()


try:
    for line in sys.stdin:
        phrase += line
except KeyboardInterrupt:
    sys.stdout.flush()
    pass



if len(phrase)<=0:
    print("empty phrase", file=sys.stderr)
    sys.exit(1)

words = phrase.split()
indexes = []

#validate string
for word in words:
    try:
        i = memo.wordlist.index(memo.normalize_string(word))
        if i<0:
            raise ValueError("invalid index value")
        indexes.append(str(i+1).rjust(4, '0'))
    except:
        print("word not in dictionary :", word, file=sys.stderr)
        sys.exit(2)


#normalised phrase
phrase = ' '.join(words)


if(args.format):
    chunks = [indexes[i:i+3] for i in range(0, len(indexes), 3)]
    indexes = '\n'.join(', '.join(chunk) for chunk in chunks)
else:
    indexes = ' '.join(indexes)

if(args.verbose):
    print("\n-------------------------------------\n", file=sys.stderr)
    print(__file__, file=sys.stderr)
    print("Verose:", args.verbose, file=sys.stderr)
    print("Words:", len(words), file=sys.stderr)
    print("Phrase: ", phrase, file=sys.stderr)
    print("\nIndexes: ", file=sys.stderr)

print(indexes, end='')
print("\n", file=sys.stderr)

