#!/usr/bin/env python

import hashlib
import sys
import argparse
from mnemonic import Mnemonic

parser = argparse.ArgumentParser()
memo = Mnemonic("english")
phrase = ""

parser.add_argument("-v", dest="verbose", action="store_true", default=False)
args = parser.parse_args()

try:
    for line in sys.stdin:
        phrase += line
except KeyboardInterrupt:
    sys.stdout.flush()
    pass



if len(phrase)<=0:
    print("empty phrase", file=sys.stderr)
    sys.exit(1)

words = phrase.split()

#validate string
for word in words:
    try:
        i = memo.wordlist.index(memo.normalize_string(word))
        if i<0:
            raise ValueError("invalid index value")
    except:
        print("word not in dictionary :", word, file=sys.stderr)
        sys.exit(2)


#normalised phrase
phrase = ' '.join(words)
seed = memo.to_seed(phrase, "")


if(args.verbose):
    print("\n---------------------------------------\n", file=sys.stderr)
    print(__file__, file=sys.stderr)
    print("Verose:", args.verbose, file=sys.stderr)
    print("Words:", len(words), file=sys.stderr)
    print("Phrase: ", phrase, file=sys.stderr)
    print("Seed: ", seed.hex(), file=sys.stderr)


print(seed.hex(), end='')

#phrase2 = memo.to_mnemonic(hashlib.sha256(seed).digest())

#print(phrase2, end='')
