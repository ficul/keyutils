#!/usr/bin/env python

import hashlib
import sys
import argparse
from mnemonic import Mnemonic

parser = argparse.ArgumentParser()
memo = Mnemonic("english")
phrase = ""

parser.add_argument("-v", dest="verbose", action="store_true", default=False)
parser.add_argument("-f", dest="format", action="store_true", default=False)

args = parser.parse_args()


try:
    for line in sys.stdin:
        phrase += line
except KeyboardInterrupt:
    sys.stdout.flush()
    pass



if len(phrase)<=0:
    print("empty phrase", file=sys.stderr)
    sys.exit(1)

indexes = phrase.split()
words = []

#validate string
for indx in indexes:
    indx = int(indx) - 1
    if(indx < 0 or indx >= len(memo.wordlist)):
        print("invalid index value")
        sys.exit(1)
    words.append(memo.wordlist[indx])
        


#normalised phrase
phrase = ' '.join(words)


if(args.format):
    chunks = [words[i:i+3] for i in range(0, len(words), 3)]
    words = '\n'.join(', '.join(chunk) for chunk in chunks)
else:
    words = ' '.join(words)

if(args.verbose):
    print("\n-------------------------------------\n", file=sys.stderr)
    print(__file__, file=sys.stderr)
    print("Verose:", args.verbose, file=sys.stderr)
    print("Words:", len(indexes), file=sys.stderr)
    print("Indexes: ", " ".join(indexes), file=sys.stderr)
    print("\nWords: ", file=sys.stderr)

print(words, end='')
print("\n", file=sys.stderr)

