#!/usr/bin/env python

import hashlib
import sys
import argparse
from mnemonic import Mnemonic


seed = ""
memo = Mnemonic("english")

parser = argparse.ArgumentParser()
parser.add_argument("-v", dest="verbose", action="store_true", default=False)

args = parser.parse_args()


try:
    for line in sys.stdin:
        seed += line
except KeyboardInterrupt:
    sys.stdout.flush()
    pass

#strip whitespaces
seed = ''.join(seed.split())


#validate input string
try:
    int(seed, 16)
except ValueError:
    print("invalid hex value", file=sys.stderr)
    sys.exit(1)

seed = bytes.fromhex(seed)


if(args.verbose):
    print("\n-------------------------------------------------------\n", file=sys.stderr)
    print(__file__, file=sys.stderr)
    print("Verbose: ",args.verbose, file=sys.stderr)
    print("Seed hex:", seed.hex(), file=sys.stderr)

hash = hashlib.sha256(seed)
phrase = memo.to_mnemonic(hash.digest())
print(file=sys.stderr)
print(phrase, end='')
