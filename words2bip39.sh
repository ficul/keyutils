

words2seed="words2seed.py"
words2seed=$( [[ -f "./$words2seed" ]] && echo "./$words2seed"||echo $words2seed)

seed2bip39="seed2bip39.py"
seed2bip39=$( [[ -f "./$seed2bip39" ]] && echo "./$seed2bip39"||echo $seed2bip39)

words2index="words2index.py"
words2index=$( [[ -f "./$words2index" ]] && echo "./$words2index" ||echo $words2index )


echo "used script: $words2seed"
echo "used script: $seed2bip39"
echo "used script: $words2index"

cat| $words2seed -v| $seed2bip39 -v| tee >(cat >&2)|$words2index -v -f
