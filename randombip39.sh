


if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <number_of_characters> <number_of_random_bits>"
    exit 1
fi


seed2bip39="seed2bip39.py"
seed2bip39=$([[ -f "./$seed2bip39" ]] && echo ./"$seed2bip39" || echo "$seed2bip39")

words2index="words2index.py"
words2index=$([[ -f "./$words2index" ]] && echo "./$words2index" || echo "$words2index")

echo "script used: $seed2bip39"
echo "script used: $words2index"

{
    head -c "$2" /dev/urandom;  # Append random bytes
    head -c "$1"; # Read fom stdin
} | od -v -t x1 -An|tee >(cat >&2)| $seed2bip39 -v | tee >(cat >&2)| $words2index  -f -v
