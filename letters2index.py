#!/usr/bin/env python

import hashlib
import sys
import argparse
from mnemonic import Mnemonic

parser = argparse.ArgumentParser()
memo = Mnemonic("english")
phrase = ""

parser.add_argument("-v", dest="verbose", action="store_true", default=False)
parser.add_argument("-f", dest="format", action="store_true", default=False)

args = parser.parse_args()


try:
    for line in sys.stdin:
        phrase += line
except KeyboardInterrupt:
    sys.stdout.flush()
    pass



if len(phrase)<=0:
    print("empty phrase", file=sys.stderr)
    sys.exit(1)

words = phrase.split()
indexes = []

#validate string
for word in words:
    indx=""
    for ch in word:
        digit = ord(ch.upper()) - 65;
        if(digit<0 or digit > 9):
            print("Invaild digit: ",digit)
            sys.exit(1)
        indx += str(digit)
    
    indexes.append(indx)
        



if(args.format):
    chunks = [indexes[i:i+3] for i in range(0, len(indexes), 3)]
    indexes = '\n'.join(', '.join(chunk) for chunk in chunks)
else:
    indexes = ' '.join(indexes)

if(args.verbose):
    print("\n-------------------------------------\n", file=sys.stderr)
    print(__file__, file=sys.stderr)
    print("Verose:", args.verbose, file=sys.stderr)
    print("Words:", len(words), file=sys.stderr)
    print("Words: ", " ".join(words), file=sys.stderr)
    print("\nIndexes: ", file=sys.stderr)

print(indexes, end='')
print("\n", file=sys.stderr)

